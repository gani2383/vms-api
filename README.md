This application is Vendor Management System.

Here we can add, update, read and delete the venodrs by ID.

Also we can search the vendors list by name or PAN or phone number.



Instructions to run the process:

git clone 'Repository URL'

npm install

node app.js


Use POSTMAN to send request to the API

Server URL : "http://localhost:3000"

To create vendor => POST localhost:3000/vendor
TO read vendor => GET localhost:3000/vendor/read/:id
To update vendor => PUT localhost:3000/vendor/update/:id
To delete vendor => DELETE localhost:3000/vendor/delete/:id
To Read all vendors => GET localhost:3000/vendor/all
To read vendor by PAN => GET localhost:3000/vendor/pan/:pan
To search vendor by name or PAN or phone number => GET localhost:3000/vendor/search?value=<any value>


sample vendor object : 
{
	"name":"Ganesh",
	"PAN":"ABC123",
	"phoneNumber":"123456789",
	"paymentMode": "Prepaid"
}


