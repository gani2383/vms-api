const express=require('express');
const app=express();
const port=process.env.PORT;
require('./db/database');

app.use(express.json());

const vendorRouter=require('./routers/vendorRouter');
app.use(vendorRouter);

app.listen(port, ()=>{
    console.log('App is listening to port: ' + port);
})