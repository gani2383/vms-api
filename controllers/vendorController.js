const Vendor=require('../models/vendorModel');

const controller= {

    createVendor : async (req,res)=>{
        try {
            const vendor=new Vendor(req.body);
            const result=await vendor.save();
            res.status(200).send(result);
        } catch (error) {
            res.status(400).send(error);
        }
    },

    readVendor : async (req,res)=>{
        try {
            const vendor=await Vendor.findById(req.params.id);
            if(!vendor){
                return res.status(404).send('No vendor exist with given id')
            }
            res.status(200).send(vendor);
        } catch (error) {
            res.status(400).send(error);
        }
    },

    updateVendor : async (req,res)=>{
        try {
            const vendor=await Vendor.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true});
            if(!vendor){
                return res.status(404).send('No vendor exist with given id')
            }
            res.status(200).send({"message": "Vendor has been updated", "vendor":vendor});
        } catch (error) {
            res.status(400).send(error);
        }
    },

    deleteVendor : async (req,res)=>{
        try {
            const vendor=await Vendor.findByIdAndDelete(req.params.id);
            if(!vendor){
                return res.status(404).send('No vendor exist with given id')
            }
            res.status(200).send({"message": "Vendor has been deleted", "vendor":vendor});
        } catch (error) {
            res.status(400).send(error);
        }
    },

    getAllVendors : async (req,res)=>{
        try {
            const vendors=await Vendor.find({});
            if(vendors.length==0){
                return res.status(404).send('No vendor exist')
            }
            res.status(200).send(vendors);
        } catch (error) {
            res.status(400).send(error);
        }
    },

    searchVendor : async (req,res)=>{
        try {
            const vendors=await Vendor.find({$or: [{"name" : req.query.value},{"PAN": req.query.value},{"phoneNumber":req.query.value}]})
            if(vendors.length==0){
                return res.status(404).send('No vendor exist')
            }
            res.status(200).send(vendors);
        } catch (error) {
            res.status(400).send(error);
        }
    },

    checkVendorByPAN : async (req,res)=>{
        try {
            const vendor=await Vendor.find({"PAN": req.params.pan});
            if(vendor.length==0){
                return res.status(404).send('No vendor exist with given PAN')
            }
            res.status(200).send(vendor);
        } catch (error) {
            res.status(400).send(error);
        }
    }
}

module.exports=controller;