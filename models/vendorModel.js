const mongoose=require('mongoose');

const vendorSchema=new mongoose.Schema({
    name : {
        type: String, 
        required:true
    },
    PAN : {
        type: String, 
        required: true,
        unique : true
    },
    phoneNumber : {
        type: String, 
        required: true
    },
    createdDate : {
        type: Date, default: Date.now()
    },
    paymentMode : {
        type: String, 
        enum:['Prepaid', 'Postpaid'], 
        required: true
    }
},
{
    timestamps : true
})

const Vendor=mongoose.model('vendors', vendorSchema);

module.exports=Vendor;