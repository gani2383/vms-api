const express=require('express');
const router=new express.Router();
const controller=require('../controllers/vendorController');

router.post('/vendor', controller.createVendor);
router.get('/vendor/read/:id', controller.readVendor);
router.put('/vendor/update/:id', controller.updateVendor);
router.delete('/vendor/delete/:id', controller.deleteVendor);
router.get('/vendor/all', controller.getAllVendors);
router.get('/vendor/pan/:pan', controller.checkVendorByPAN);
router.get('/vendor/search', controller.searchVendor);
module.exports=router;